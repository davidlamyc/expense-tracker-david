# Expense Tracker (React)

I am using this to practice bitbucket pipelines. This was cloned from the awesome Brad Traversy's repo.

This is a React version of the [vanilla JS Expense Tracker](https://github.com/bradtraversy/vanillawebprojects/tree/master/expense-tracker). It uses functional components with hooks and the context API

## Usage
```
npm install

# Run on http://localhost:3000
npm start

# Build for prod
npm run build
```

